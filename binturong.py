#!/usr/bin/env python3

import argparse
import gettext as gettext_
import glob
import os
import sys
import json

RELEASES = ["homepage", "42"]


def convert_to_python(filename):
    with open(filename) as f:
        paragraphs = f.read().split("\n\n")

    if paragraphs and paragraphs[0][:3] == "---":
        lines = paragraphs.pop(0).split("\n")
        lines.insert(1, "lang: { lang_ietf }")
        front_matter = "\n".join(lines) + "\n\n"
    else:
        front_matter = ""

    definitions = ""
    content_list = []

    for n, p in enumerate(paragraphs):
        var_name = f"p{ n }"
        var_value = beautified_repr(p)
        definitions += f"{ var_name } = gettext({ var_value })\n"
        content_list.append(f"{{ { var_name } }}")

    content = front_matter + "\n\n".join(content_list)

    return f"{ definitions }\ntranslated = f'''{ content }'''"


def beautified_repr(obj):
    s = repr(obj)
    quote = s[0]
    if quote in ['"', "'"]:
        s = f"{ quote }{ quote }{ s }{ quote }{ quote }".replace(r"\n", "\n")

    return s


def py_filename(filename):
    return f"{ filename }.i18n.py"


def generate(release, files):
    for filename in files:
        result = convert_to_python(filename)
        with open(py_filename(filename), "w") as f:
            f.write(result)

    with open(f"po-{ release }/POTFILES.in", "w") as f:
        filenames = "\n".join(sorted(map(py_filename, files)))
        f.write(filenames)


def arg_parse():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(required=True)

    subparsers.add_parser(
        "content-changed",
        help="Call this after changing or adding markdown files and commit result to repo.",
    ).set_defaults(func=cmd_content_changed)

    subparsers.add_parser(
        "generate-translations",
        help="Use this in CI to generate translated markdown files.",
    ).set_defaults(func=cmd_generate_translations)

    args = parser.parse_args()
    args.func()


def cmd_content_changed():
    for release in RELEASES:
        if release == "homepage":
            files = list(filter(lambda x: x.count(".") < 2, glob.glob(f"*.md")))
            files.remove("README.md")
        else:
            files = list(
                filter(
                    lambda x: x.count(".") < 2,
                    glob.glob(f"{ release }/**/*.md", recursive=True),
                )
            )

        generate(release, files)


def generate_translations(release):
    with open(f"po-{ release }/POTFILES.in") as f:
        py_files = f.read().split("\n")

    with open(f"po-{ release }/LINGUAS") as f:
        langs = f.read().split("\n")

    json_string = json.dumps(
        list(map(lambda x: {"lang_ietf": x.replace("_", "-")}, langs))
    )
    with open(f"_data/languages_{ release }.json", "w") as f:
        f.write(json_string)

    for lang in langs:
        if not lang:
            continue

        lang_ietf = lang.replace("_", "-")

        gettext = gettext_.translation(
            f"release-notes-{ release }",
            localedir="install/share/locale",
            languages=[lang],
        ).gettext

        for py_file in py_files:
            if not py_file:
                continue

            print(lang, py_file)

            with open(py_file) as f:
                locals_ = locals()
                exec(f.read(), globals(), locals_)
                translated = locals_["translated"]

            with open(f"{ py_file[:-11] }.{ lang }.md", "w") as f:
                f.write(translated)


def cmd_generate_translations():
    for release in RELEASES:
        generate_translations(release)


def main():
    arg_parse()

    return 0


if __name__ == "__main__":
    sys.exit(main())
