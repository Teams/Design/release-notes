# Release Notes

Easy to maintain and beautiful website for GNOME Release Notes. Live at [release.gnome.org](https://release.gnome.org).

## Notes for authors

After changing `.md`-files, please execute `./binturong.py content-changed`. The generated `.py`-files have to be added to the repository to be picked up for translation.

## Goals

+ Image and video heavy (emphasize GNOME look, lots of screenshots and [illustraton style](https://gitlab.gnome.org/Teams/Design/app-illustrations/)).
+ Easy to write (markdown rather than html). Big fan of preview in Apostrophe, so favoring markdown.

## Prior Art
* [Blender release notes](https://www.blender.org/download/releases/)
* [Elementary release notes (blog)](https://blog.elementary.io/elementary-os-6-odin-released/)

## Setup & build
Pages are automatically deployed using gitlab CI. To develop/test locally on [Fedora Silverblue](https://fedoraproject.org/silverblue/) using [Toolbx](https://containertoolbx.org):

1) Set up toolbox container and clone the repo
```
toolbox create RN --release 39
toolbox enter RN
mkdir src && cd src
git clone git@ssh.gitlab.gnome.org:Teams/Design/release-notes.git
cd release-notes
```

2) Set up ruby and install rvm
```
sudo dnf install -y curl gcc-c++
sudo dnf builddep -y ruby
\curl -sSL https://get.rvm.io | bash -s stable
echo "source ~/.rvm/scripts/rvm" >> $HOME/.bash_profile
source $HOME/.bash_profile
rvm install ruby-3.1.2
bundle install
bundle exec jekyll s
```

## Translations

- Translations are currently broken. Help appreciated.
