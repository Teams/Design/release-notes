---
layout: default
image: 42/card.png
releasepage: true
languages: languages_42
urlprepend: "../"
---

# Introducing GNOME&nbsp;42

GNOME 42 is the product of 6 months' work by the GNOME project. It includes a significant number of new features, as well as many enhancements and smaller improvements.

### Dark Style Preference

A new global dark UI style preference has arrived in GNOME for version 42. This can be used to request that apps use a dark UI style instead of light. The new setting can be found in the new Appearance panel in the Settings app, and is followed by most GNOME apps.

<video controls autoplay muted loop>
  <source src="https://static.gnome.org/release/42/appearance.webm" type="video/webm">
  <source src="https://static.gnome.org/release/42/appearance.mp4" type="video/mp4">
</video>

All of GNOME's wallpapers support the dark style preference, so that switching to dark style also switches to a darker wallpaper style.

Under the hood, the new dark style makes it possible for apps to provide their own style settings that are independent of the system style preference. This makes it possible to switch individual apps to dark or light, if you prefer to use them that way.

## New Screenshot Experience

GNOME 42 comes with a redesigned screenshot feature. This makes it super easy to take screenshots and screen recordings, all from the same tool. Just press <kbd>Print Screen</kbd> and an interactive overlay launches, with options for the area to capture and whether to create an image or a recording.

<video controls muted>
  <source src="https://static.gnome.org/release/42/screenshots.webm" type="video/webm">
  <source src="https://static.gnome.org/release/42/screenshots.mp4" type="video/mp4">
</video>

The new screenshot feature allows taking screenshots of different screen areas, as well as individual windows. It's also possible to create screen recordings of selected screen areas.

There are a collection of handy keyboard shortcuts that can be used with the screenshot feature, and these can be a quick way to perform common actions. These include:

{: .shortcuts}
 * <kbd>S</kbd> - select area
 * <kbd>C</kbd> - capture screen
 * <kbd>W</kbd> - capture window
 * <kbd>P</kbd> - show/hide pointer
 * <kbd>V</kbd> - screenshot/screen recording
 * <kbd>Enter</kbd> / <kbd>Space</kbd> / <kbd>Ctrl</kbd>+<kbd>C</kbd>- capture 

## Upgraded Apps

GTK 4 and libadwaita provide next generation capabilities for GNOME applications, and many GNOME apps have started to use these components for GNOME 42. As a result, these apps have better performance, a new modern UI style, and new user interface elements.

Core GNOME apps which have been ported to GTK 4 and libadwaita include:

- [![baobab](https://apps.gnome.org/icons/scalable/org.gnome.baobab.svg){: .icon-dropshadow} <br>Disk Usage Analyzer](https://apps.gnome.org/app/org.gnome.baobab/)
- [![fonts](https://apps.gnome.org/icons/scalable/org.gnome.font-viewer.svg){: .icon-dropshadow} <br>Fonts](https://apps.gnome.org/app/org.gnome.font-viewer/)
- [![](org.gnome.Todo.svg){: .icon-dropshadow} <br>To&nbsp;Do](https://flathub.org/apps/details/org.gnome.Todo)
- [![Tour](https://apps.gnome.org/icons/scalable/org.gnome.Tour.svg){: .icon-dropshadow} <br>Tour](https://apps.gnome.org/app/org.gnome.Tour/)
- [![Tour](https://apps.gnome.org/icons/scalable/org.gnome.Calendar.svg){: .icon-dropshadow} <br>Calendar](https://apps.gnome.org/app/org.gnome.Calendar/)
- [![clocks](https://apps.gnome.org/icons/scalable/org.gnome.clocks.svg){: .icon-dropshadow} <br>Clocks](https://apps.gnome.org/app/org.gnome.clocks/)
- [![software](https://apps.gnome.org/icons/scalable/org.gnome.Software.svg){: .icon-dropshadow} <br>Software](https://apps.gnome.org/app/org.gnome.Software/)
- [![characters](https://apps.gnome.org/icons/scalable/org.gnome.Characters.svg){: .icon-dropshadow} <br>Characters](https://apps.gnome.org/app/org.gnome.Characters/)
- [![contacts](https://apps.gnome.org/icons/scalable/org.gnome.Contacts.svg){: .icon-dropshadow} <br>Contacts](https://apps.gnome.org/app/org.gnome.Contacts/)
- [![Weather](https://apps.gnome.org/icons/scalable/org.gnome.Weather.svg){: .icon-dropshadow} <br>Weather](https://apps.gnome.org/app/org.gnome.Weather/)
- [![Calculator](https://apps.gnome.org/icons/scalable/org.gnome.Calculator.svg){: .icon-dropshadow} <br>Calculator](https://apps.gnome.org/app/org.gnome.Calculator/)
{: .icon-grid.small}

The newly upgraded versions of many of these apps can be installed using Flatpak - open the links above to find out more.

The biggest app to upgraded to be GTK 4 and libadwaita is Settings. This major piece of work gives the Settings app a refreshed look and feel. Many of the settings panels have had design updates as part of this upgrade work, including Applications, Appearance, Display, Region & Language, Users, and Wacom Tablet.

Many other apps have also been ported to GTK 4 and libadwaita, such as [Sound Recorder](https://flathub.org/apps/details/org.gnome.SoundRecorder), [App Icon Preview](https://apps.gnome.org/app/org.gnome.design.AppIconPreview/), [Icon Library](https://apps.gnome.org/app/org.gnome.design.IconLibrary/), and [Secrets](https://apps.gnome.org/app/org.gnome.World.Secrets/). More apps are in the process of being upgraded.

## New Default Apps

GNOME's default apps is the set of apps that GNOME recommends be included in a default GNOME installation. For GNOME 42, we have two new apps in the default app set: Text Editor and Console.

Text Editor has previously been made available as a preview release, and is now being recommended as the default GNOME text editor. This GTK 4 text editor has a slick UI and automatically saves your work, to prevent you from losing it.

<picture>
	<source srcset="text-editor-screenshot.webp" type="image/webp">
	<source srcset="text-editor-screenshot.png" type="image/png">
	<img src="text-editor-screenshot.png" alt="Text Editor">
</picture>

Console is a brand new terminal app. It has some nice UI touches, such as overlaid scroll bars, an overlaid size indicator, and a header bar that changes color to indicate when running as root.

<picture>
	<source srcset="console-screenshot.webp" type="image/webp">
	<source srcset="console-screenshot.png" type="image/png">
	<img src="console-screenshot.png" alt="Console">
</picture>

Both Text Editor and Console have a new, modern tab UI. Both also support the dark UI style, and have their own style controls to allow you to change between dark and light individually for each app.

## Performance Improvements

GNOME 42 includes a valuable set of performance improvements, as the GNOME community continues its work to enhance system speed and resource usage.

 * [Videos](https://apps.gnome.org/app/org.gnome.Totem/) is now using modern OpenGL widgets with hardware accelerated decoding, resulting in much smoother video playback.
 * [File indexing in Tracker has been dramatically improved](https://discourse.gnome.org/t/tracker-3-3-will-reindex-files-and-change-content-ids/8583), with faster startup times and reduced memory usage.
 * [Input handling has been significantly enhanced](https://blogs.gnome.org/shell-dev/2021/12/08/an-eventful-instant/), resulting in lower input latency and improved responsiveness when the system is under load. This will be particularly beneficial for games and graphics applications.
 * Web, the GNOME browser, now enables hardware accelerated rendering on all websites, resulting in more fluid scrolling.
 * Improvements in how fullscreen apps are rendered will result in reduced energy consumption for video playback and increased frame rates for games.
 
## RDP Support

<picture>
	<source srcset="rdp-screenshot.webp" type="image/webp">
	<source srcset="rdp-screenshot.png" type="image/png">
	<img src="rdp-screenshot.png" alt="RDP Settings">
</picture>

GNOME's existing remote desktop feature has been upgraded to support the RDP protocol. This offers a more secure and featureful remote desktop experience, for those wanting to remotely connect to a GNOME system.

Remote desktop connections using RDP can be enabled in the Settings app, from the Sharing panel. The new feature aims to make it simple and easy to start using RDP for remote desktop, and automatically take care of configuration and setup.

GNOME's new RDP integration effectively replaces the previous support for VNC, though users can continue to setup and use VNC manually, should they want to.

## And That's Not All...

GNOME 42 includes many other smaller improvements and enhancements.

<picture>
	<source srcset="shell42-screenshot.webp" type="image/webp">
	<source srcset="shell42-screenshot.jpg" type="image/jpeg">
	<img src="shell42-screenshot.jpg" alt="Shell Styling">
</picture>

 * The styling of GNOME's system UI has been updated. Many of these changes are subtle, and result in a more polished and elegant appearance. As part of this change, the style of GNOME's symbolic icons has also been refreshed.
 * The Files app has a new scrollable path bar, an updated renaming interface, and updated icons.
 * Boxes, GNOME's virtual machine app, has a redesigned preferences view and has better support for modern UEFI operating systems.
 * In Videos, media playback can now by controlled through the integrated media controls in the notifications list.

## Developer Experience

GNOME 42 includes significant new features and improvements for developers who are using the GNOME platform. Read the developers section to learn more.

<ul class="linkdiv">
  <li>
    <a href="developers/"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 42

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 42 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using     the [Boxes](https://apps.gnome.org/app/org.gnome.Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](https://welcome.gnome.org).
