---
layout: default
image: 47.png
languages: languages_47
urlprepend: "../"
redirect_from:
- /latest
- /stable
---

# Introducing GNOME&nbsp;47, “Denver”

**September 18, 2024**

The GNOME project is thrilled to unveil GNOME 47, the newest release that reflects six months of dedicated effort from our community. A huge thank you to all the contributors who made this possible!

This release, dubbed “Denver,” honors the incredible efforts of the GUADEC 2024 organizers. Join us as we delve into the exciting new features of GNOME 47.

## Accent Colors

Accent colors have finally arrived! This new option allows customization of the existing blue accent color which is used throughout GNOME's interfaces, with a range of fantastic alternative accent colors to choose from. Just select one from the Appearance settings, and it will be automatically used throughout the system and apps. This new personalization feature is a great way to make your system look and feel like it is yours.

<video nocontrols muted autoplay loop class="rounded">
  <p>Color customisation in the Settings.</p>
  <source src="accent-colors.webm" type="video/webm">
  <source src="accent-colors.mp4" type="video/mp4">
</video>

## System Enhancements

GNOME 47 comes with a set of significant system enhancements.

### Enhanced Small Screen Support

GNOME 47 enhances the user experience on screens with lower resolutions by optimizing how icons and interface elements are rendered. In previous versions, users with lower-resolution displays might have noticed that icons appeared too small or difficult to interact with. With this update, GNOME automatically scales icons to appear larger and more visually accessible on such screens.

<picture class="rounded beforeafter">
    <img src="dash-after.webp" alt="After: Large icons in the dash." class="after-img">
    <img src="dash-before.webp" alt="Before: Tiny icons in the dash.">
</picture>
<small>Before/after: In older GNOME releases the size of app icons in the dash was not optimal on small displays.</small>

### Screen Capture Hardware Encoding

GNOME 47 introduces support for hardware encoding on Intel and AMD GPUs when recording the screen. This significantly reduces the strain on your system during screen recording. As a result, you'll experience smoother performance with less impact on system resources, making it easier to capture high-quality screen captures without compromising the responsiveness of your desktop or other running applications. Whether you're creating tutorials, recording gameplay, this improvement ensures a more efficient and seamless screen capture experience.

### Faster, More Accurate Rendering

This release also brings significant improvements to GTK rendering, especially for older hardware and mobile devices. Users on legacy systems or mobile phones will notice a more responsive and visually consistent experience, with reduced lag and better overall graphical fidelity.

### Persistent Remote Desktop Sessions

GNOME's remote desktop support has improved for version 47, with the addition of persistent remote login sessions. If you happen to be disconnected from a remote login session, it will now continue, so that it is possible to log back in and continue from where you left off, with the system in the same state as when you left it.

### New Style Dialog Windows

Dialog windows have a new and updated design for GNOME 47. Found in both system and app dialogs, the new layout enhances usability across various screen sizes, by ensuring that dialogs now function more effectively on narrow screens and mobile devices. Whether you're working on a compact laptop or using GNOME on a smartphone, these improvements make dialogs more accessible and easier to interact with, adapting seamlessly to different display environments. The new style dialogs also look great with custom accent colors.

<picture class="rounded">
    <source srcset="dialog-dark-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="dialog-light-screenshot.webp">
</picture>

##  New Open and Save File Dialogs

GNOME 47 comes with brand new file open and save file dialogs. The new dialogs are a major upgrade compared with the previous versions, and are based on the existing Files app rather than being a separate codebase. This results in the new dialogs having a much more complete set of features compared with the old open and save dialogs. With the new dialogs you can zoom the view, change the sort order in the icon view, rename files and folders, preview files, and more.

Another major advantage of the new file dialogs is how they are consistent with the Files app. The new dialogs all look and behave the same way as in Files, the locations in the sidebar are consistent, and search returns the same results in the same order.

The new file dialogs are also more capable, with on-demand thumbnail generation and faster and more comprehensive search.

<picture class="rounded">
  <source srcset="filedialog-d-screenshot.webp" media="(prefers-color-scheme: dark)">
  <img src="filedialog-l-screenshot.webp">
</picture>

## Improved Files App

The Files app includes a major set of improvements in GNOME 47.

### Improved Navigation

Files has several significant navigation improvements for GNOME 47. First, a new "Network" view provides a better way to browse remote file locations, with a clear list that is divided into sections for currently connected, previously used, and available network locations. This view is easier to understand and use than the previous "Other Locations" view.

<picture class="rounded">
    <source srcset="nautilus-network-d-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="nautilus-network-l-screenshot.webp">
</picture> 

Second, physical internal disks are now all listed in the sidebar, keeping them together with other disks and making them easy to access.

Third and finally, a reworked places sidebar allows more of the default sidebar items to be removed, including the locations for Documents, Downloads, Music and Videos. Being able to remove these items gives more space in the sidebar for customization.

### Better Search Information

GNOME 47 introduces improvements to Files search, by providing more contextual information about search performance. If any factors are affecting search results, such as if the location is not indexed or if it is a remote directory, then a new button is shown in the search bar, which shows information about the current search context. This added context helps users understand why certain searches might be slower or incomplete. It also allows users to update their search settings to get the best experience.

### Modernized Interface

Several parts of the Files app have also received a refreshed interface for GNOME 47. This includes updated new folder and file compression dialogs.

## Settings

The latest release of the Settings app includes a number of helpful additions.

**Activate windows on hover**: in the Accessibility settings, a new "activate windows on hover" option has been added.  In previous releases, this option was only available through third party apps. Now with GNOME 47, it's accessible directly in the Settings.

**Input source previewing**: in the Keyboard settings, it is now possible to preview input sources from the add input source dialog. This enhancement allows users to see a visual representation of each keyboard layout before selecting it.

**Mobile suspend options**: in the Power settings, a different set of suspend times are now presented for mobile devices. This enhancement allows for optimized power management, helping to improve battery life.

<picture class="rounded">
    <source srcset="window-h-d-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="window-h-l-screenshot.webp">
</picture> 

This latest release also includes a great collection of modernization improvements, with many settings panels having been upgraded to use the latest interface components, giving a more cohesive and contemporary look.

<picture class="rounded">
    <source srcset="users-d-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="users-l-screenshot.webp">
</picture>

## Improved Online Accounts

GNOME's Online Accounts features have been enhanced for the latest version.

- IMAP/SMTP email account details now automatically complete based on the address used
- Kerberos accounts use less power on an ongoing basis
- Email, calendar and contact integration has been added to Microsoft 365 accounts
- When setting up WebDAV accounts, available services are now automatically discovered, to provide a more streamlined setup experience

## Web

Version 47 is an exciting release for the GNOME web browser, and includes multiple new features and enhancements.

**Automatically Fill Forms:** a new auto-fill feature allows you to automatically complete forms based on previous responses. This feature can be enabled from the Privacy preferences, where you can set the information to be automatically provided, like name, address, and contact information.

**Redesigned Bookmarks:** Web has a slick new bookmarks sidebar, which provides a better experience for browsing and searching your bookmarks.
  
  <picture class="rounded">
    <source srcset="bookmarks-d-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="bookmarks-l-screenshot.webp">
  </picture> 

**Privacy Reports**: a new privacy report features has been added, which displays how many trackers have been blocked by GNOME Web. Making this previously hidden information available allows you to stay informed about your online privacy.

Something to be aware of: due to changes in Firefox's account authentication process, Firefox Sync support has been disabled in version 47, as well as 46.3, and 45.4. Unfortunately, there is no estimate for when Firefox Sync might return. For more details, refer to the [issue report](https://gitlab.gnome.org/GNOME/epiphany/-/issues/2337).

## Calendar

The GNOME 47 version of the Calendar app comes packed with almost 50 bug fixes, as well as plenty of extra polish. The most noticeable change is a reworked event details popover. The new design has many advantages over the previous version, including:

* Improved handling for read-only events, which are now indicated by a lock icon
* A better-looking and more usable layout, with clearer sections and consistent spacing
* Video meeting links are now only displayed once
* Placeholders indicate when information is missing

In addition, the event importer and event editor dialogs now gracefully handle hidden and read-only calendars, and the "Add Calendar" dialog has also been tweaked to be clearer, and to catch and display errors.

## Welcome, Circle Friends!

[GNOME Circle](https://circle.gnome.org/) is a group of fantastic apps for GNOME, which the GNOME project promotes and supports. Since GNOME 46 was released, these new apps have been added:

- [![Binary](io.github.fizzyizzy05.binary.svg){: .icon-dropshadow} <br>Binary](https://apps.gnome.org/Binary/)
- [![Biblioteca](app.drey.Biblioteca.svg){: .icon-dropshadow} <br>Biblioteca](https://apps.gnome.org/Biblioteca/)
- [![Hieroglyphic](io.github.finefindus.Hieroglyphic.svg){: .icon-dropshadow} <br>Hieroglyphic](https://apps.gnome.org/Hieroglyphic/)
- [![Resources](net.nokyan.Resources.svg){: .icon-dropshadow} <br>Resources](https://apps.gnome.org/Resources/)
- [![Tuba](dev.geopjr.Tuba.svg){: .icon-dropshadow} <br>Tuba](https://apps.gnome.org/Tuba/)
- [![Valuta](io.github.idevecore.Valuta.svg){: .icon-dropshadow} <br>Valuta](https://apps.gnome.org/Valuta/)
{: .icon-grid}

Welcome to these new members of the GNOME community!

## Other Changes

GNOME 47 also comes with many other smaller improvements.

* Disk Usage Analyzer has a refreshed interface for GNOME 47. The updated look includes a modernized files list, updated icons, and a new look location bar.
* Wait cursors have been refreshed to adjust to the new spinner widgetry in the platform.
* Maps uses vector tiles by default now.
* App recommendations have been updated in Software, making it easy to find the latest and greatest apps to install
* Maps also features public transit routing in selected locations. Rather than relying on commercial services, Maps leverages a community-run transport [routing service](https://transitous.org/).

  <picture>
    <source srcset="public-transport-d-screenshot.webp" media="(prefers-color-scheme: dark)">
    <img src="public-transport-l-screenshot.webp">
  </picture> 

This release also includes a number of technical changes which are either experimental or will be the basis of other work in the future:

* GNOME 47 includes an enhanced fractional display scaling feature, which provides better support for legacy X11 apps. This feature is still considered experimental and should only be used for testing. To enable it, you can run the following from the command line: ``gsettings set org.gnome.mutter experimental-features '["scale-monitor-framebuffer", "xwayland-native-scaling"]'
``
* The latest GNOME release introduces the ability to play games that uses Virtual Reality (VR) headsets, while using Wayland desktop sessions.
* The foundations for reliable and hardware accelerated screen sharing needed by the proprietary NVIDIA driver has been added in GNOME 47, opening the door for applications to improve screen sharing and screen recording performance in the future.

## Developer Experience

GNOME 47 brings a range of new features and enhancements for developers working with the GNOME platform. Explore the developer section for detailed insights.

<ul class="linkdiv">
  <li>
    <a href="developers/index.html"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 47

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 47 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using the [Boxes](https://apps.gnome.org/Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](https://welcome.gnome.org/).
