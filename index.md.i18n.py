p0 = gettext('''# GNOME Release Notes''')
p1 = gettext('''<a href="42/">
\t<picture class="large">
\t\t<source srcset="42.webp" type="image/webp">
\t\t<source srcset="42.jpg" type="image/jpeg">
\t\t<img src="42.jpg" alt="GNOME 42">
\t</picture>
</a>''')
p2 = gettext('''## Previous Releases''')
p3 = gettext('''<div class="tiles">
<a href="https://help.gnome.org/misc/release-notes/41.0/">
\t<picture>
\t\t<source srcset="41.webp" type="image/webp">
\t\t<source srcset="41.jpg" type="image/jpeg">
\t\t<img src="41.jpg" alt="GNOME 41">
\t</picture>
</a>
<a href="https://help.gnome.org/misc/release-notes/40.0/">
\t<picture>
\t\t<source srcset="40.webp" type="image/webp">
\t\t<source srcset="40.jpg" type="image/jpeg">
\t\t<img src="40.jpg" alt="GNOME 40">
\t</picture>
</a>
<a href="https://help.gnome.org/misc/release-notes/3.38/">
\t<picture>
\t    <source srcset="3.38.webp" type="image/webp">
\t    <source srcset="3.38.jpg" type="image/jpeg">
\t    <img src="3.38.jpg" alt="GNOME 3.38">
\t</picture>
</a>
</div>
''')

translated = f'''---
lang: { lang_ietf }
layout: default
home: true
languages: languages_homepage
---

{ p0 }

{ p1 }

{ p2 }

{ p3 }'''