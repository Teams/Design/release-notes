---
layout: default
image: 44.jpg
urlprepend: "../../"
languages: languages_42
---

<p class="full" markdown="1">
![Devel Docs](developers.svg)
</p>

# GNOME 44 for Developers

GNOME 44 comes with a host of new capabilities and features for those using the GNOME platform.

## GTK

GNOME 44 is accompanied by the latest stable GTK release, version 4.10. This version includes some significant features and changes for developers.

### Deprecations

GTK 4.10 deprecates a range of existing APIs. These APIs will continue to be available for the 4.x release series, but will be removed in the next major GTK version, which will be 5.0. The GTK development blog provides [an overview of the changes and what they mean](https://blog.gtk.org/2022/10/30/on-deprecations/).

#### Cell Renderers

GTK 4.10 deprecates all APIs relating to cell renders, including `GtkTreeView`, `GtkIconView`, and `GtkComboBox`. Alternatives to these widgets already exist, in the shape of [GtkColumnView](https://docs.gtk.org/gtk4/class.ColumnView.html), [GtkGridView](https://docs.gtk.org/gtk4/class.GridView.html), and [GtkDropDown](https://docs.gtk.org/gtk4/class.DropDown.html). 

#### Dialogs

GTK 4.10 deprecates existing chooser APIs, including: `GtkDialog`, `GtkFileChooserDialog`, `GtkFileChooserWidget`, `GtkFileChooser`, `GtkFileChooserNative`, `GtkColorChooserDialog`, `GtkColorChooserWidget`, `GtkColorButton`, `GtkColorChooser`, `GtkFontChooserDialog`, `GtkFontChooserWidget`, `GtkFontButton`, `GtkFontChooser`, `GtkAppChooserDialog`, `GtkAppChooser`, `GtkAppChooserWidget`, `GtkAppChooserButton`, and `GtkMessageDialog`.

For those looking for alternatives, new replacement APIs have been added, which work as asynchronous calls. They are: [GtkColorDialog](https://docs.gtk.org/gtk4/class.ColorDialog.html), [GtkFontDialog](https://docs.gtk.org/gtk4/class.FontDialog.html), [GtkFileDialog](https://docs.gtk.org/gtk4/class.FileDialog.html), [GtkAlertDialog](https://docs.gtk.org/gtk4/class.AlertDialog.html). Libadwaita can also be used and includes a number of common dialogs, such as [AdwPreferencesWindow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.PreferencesWindow.html) and [AdwMessageDialog](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.MessageDialog.html).

For custom dialogs, it is recommended to derive from `GtkWindow` as opposed to `GtkDialog`.

#### Info bars

GTK 4.10 deprecates `GtkInfoBar`. Libadwaita's new [AdwBanner](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Banner.html) can be used as alternative.

### Autotools

With GTK 4.10, Autotools has been removed from the GTK 3.x branch. Those wanting to build or package GTK 3.x will therefore need to use the Meson build system. Regressions experienced by Autotools users should be reported using the GTK issue tracker.

## Libadwaita

GNOME 44 is accompanied by Libadwaita 1.3, which includes three new widgets, as well as a host of bug fixes. The new widgets for 1.3 are:

[AdwBanner](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Banner.html), a narrow bar that can be used to show status information. Banners have a title, which can be centered or left-aligned, and can include a single optional button.

<picture>
	<source srcset="banner.png" type="image/png">
	<img src="banner.png" alt="Banner screenshot">
</picture>

[AdwTabOverview](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.TabOverview.html), which displays tabs from an `AdwTabView` in a grid, with a thumbnail for each tab. Search is provided, so that users can filter the view.

<picture>
	<source srcset="tab-overview.png" type="image/png">
	<img src="tab-overview.png" alt="Tab overview screenshot">
</picture>

[AdwTabButton](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.TabButton.html), a button that shows the number of open tabs in an `AdwTabView`. It is intended to be used as a open tab indicator when there's no tab bar. Typically it will open an `AdwTabOverview` when pressed.

<picture>
	<source srcset="tab-button.png" type="image/png">
	<img src="tab-button.png" alt="Tab button screenshot">
</picture>

### GLib

GNOME 44 is accompanied by GLib 2.76. Changes since the last stable version, include:

* The addition of `GPathBuf`, a new path building API which can be used to programmatically build paths on both Unix-like operating systems and Windows.
* Support for optimized static strings in [g_str_has_prefix()](https://docs.gtk.org/glib/func.str_has_prefix.html) and [g_str_has_suffix()](https://docs.gtk.org/glib/func.str_has_suffix.html). The [request for this feature](https://gitlab.gnome.org/GNOME/glib/-/issues/24) was 18 years old!
* `g_autofd`: a new attribute which can be used to automatically close file descriptors when exiting a scope, like `g_autofree` and `g_autoptr()` (see [the merge request](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3007)).
* Wrappers for the upcoming C23 API for sized memory allocations and deallocations.
* The removal of GLib's slice allocator (GSlice) implementation, now that system allocators have finally caught up with it. The existing public `g_slice` API now calls directly into the system allocator, and we recommend library and application developers to start porting away their code from the GSlice API.
* Multiple optimizations in GLib's string buffer and logging API, to avoid unnecessary allocations.
* After nearly 8 years, `application/x-zerosize` are now returned for completely empty files. Applications that allow editing empty files will need to add `application/x-zerosize` to their list of supported MIME types.
* `gdbus-codegen` can now generate D-Bus API reference docs in Markdown, in addition to the previously supported docbook and reStructuredText. See the `gdbus-codegen` docs for [information on using this feature](https://developer-old.gnome.org/gio/stable/ch35s05.html).
* Multiple security fixes in the GVariant serialization format — see [the Discourse post](https://discourse.gnome.org/t/multiple-fixes-for-gvariant-normalisation-issues-in-glib/12835).

### Builder

Version 44 of GNOME's IDE includes a large number of enhancements and fixes, including:

Changes in this release:

 * Builder has gained support for editable shortcuts. These can individually changed from the Preferences dialog, and menus and the keyboard shortcuts window will reflect changes made by the user. 
 * SDK extension resolving has been made asynchronous so projects load much faster in conjunction with Flatpak.
 * Rust template updates.
 * The search entry now focuses the text editor when enter is pressed.
 * A number of new search providers have been added.
 * Previews of search results are now shown in the global search popover.
 * User-defined keybindings in `$XDG_CONFIG_DIR/gnome-builder/keybindings.json`.
 * Many keyboard shortcuts have been added.
 * The project tree has been ported to GtkListView and includes DnD again.
 * Lots of safety improvements under the hood.
 * New filters for the project-tree and todo panels.
 * Build systems can now alter the "srcdir" allowing for projects which are in subdirectories of their Git project to work better.
 * Support for SourceKit LSP for Swift.
 * `.buildconfig` file support for runtime environment settings and file    monitoring for tracking out-of-band updates.
 * Podman and JHBuild integration improvements

### GNOME JavaScript

[GNOME's JavaScript programming environment](https://gjs.guide/) comes with a collection of improvements for GNOME 44:
 
* New `Gio.Application.prototype.runAsync()` and `GLib.MainLoop.prototype.runAsync()` methods which perform the same function as `run()`, but return a `Promise` which resolves when the main loop ends, instead of blocking while the main loop runs. Use one of these methods if you use async operations with promises in your application. Previously, it was easy to get into a state where promises never resolved if the main loop wasn't run inside a callback. Thanks to Evan Welsh for working on this.
* New `Gio.InputStream.prototype.createSyncIterator()` and `Gio.InputStream.prototype.createAsyncIterator()` methods allow easy iteration of input streams in consecutive chunks of bytes, either with a for-of loop or a for-await-of loop.
* DBus proxy wrapper classes now have a static `newAsync()` method, which returns a `Promise` that resolves to an instance of the proxy wrapper class on which `initAsync()` has completed.
* DBus property getters can now return `GLib.Variant` instances directly, if they have the correct type, instead of returning JS values and having them be packed into `GLib.Variants`.
* `Cairo.SVGSurface.prototype.finish()` and `Cairo.SVGSurface.prototype.flush()` avoid garbage collecting issues when writing SVG surfaces to disk.

### Documentation

Several improvements have been made to GNOME's developer documentation since GNOME 43:

 * The GTK Rust docs [now cover Libadwaita](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_4.html).
 * gi-docgen, the introspection-based documentation generator used by (among others) GTK to publish its API reference, has gained the ability to show if a symbol, type, signal, or property is currently unstable. This should help visually distinguish newly added API in the reference generated directly from bleeding edge sources. The same style is also used to present when a symbol was introduced and when it was deprecated.

### Background Apps

GNOME 44 is accompanied by XDG portals 1.16.1. As part of GNOME's new background apps feature, the behavior of [the background portal](https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.Background) has changed in the latest release.

Apps which want to run without a visible window no longer need to request permission to run in the background. However, users can still withdraw an app's permission ability to run in the background, and apps which run in the background without permission will be killed.

To avoid this, apps can check whether they have permission to run in the background using the background portal's `RequestBackground` method. They can also use `SetStatus` to specify a status message that is shown in the Background Apps menu.

