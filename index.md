---
layout: default
home: true
languages: languages_homepage
---

# Latest Release

<a href="47/">
	<picture class="large">
		<source srcset="47.webp" type="image/webp">
		<source srcset="47.png" type="image/png">
		<img src="47.jpg" alt="GNOME 47">
	</picture>
</a>

## Previous Releases

<div class="tiles">
<a href="46/">
	<picture class="large">
		<source srcset="46.webp" type="image/webp">
		<source srcset="46.jpg" type="image/jpeg">
		<img src="46.jpg" alt="GNOME 46">
	</picture>
</a>
<a href="45/">
	<picture class="large">
		<source srcset="45.webp" type="image/webp">
		<source srcset="45.jpg" type="image/jpeg">
		<img src="45.jpg" alt="GNOME 45">
	</picture>
</a>
<a href="/44">
	<picture>
		<source srcset="44.webp" type="image/webp">
		<source srcset="44.jpg" type="image/jpeg">
		<img src="44.jpg" alt="GNOME 44">
	</picture>
</a>
</div>
