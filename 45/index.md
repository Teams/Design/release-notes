---
layout: default
image: 45.jpg
languages: languages_45
urlprepend: "../"
---

# Introducing GNOME&nbsp;45, “Rīga”

**September 20, 2023**

The GNOME project is excited to present the latest GNOME release, version 45. For the new version we’ve focused on refining your daily interactions, enhancing performance, and making the overall experience smoother and more efficient. From subtle design tweaks to functional upgrades, GNOME 45 is all about refining the core desktop environment you rely on.

GNOME 45 is code-named “Rīga”, in recognition of the work done by the organizers of this year’s GUADEC conference. Thank you to everyone who helped!

Let’s dive in and explore what’s new!

## Activities Indicator

<video nocontrols muted autoplay loop class="rounded">
  <source src="activities-loop.webm" type="video/webm">
  <source src="activities-loop.mp4" type="video/mp4">
</video>

One of the most noticeable changes in GNOME 45 is the new activities button. Located in the corner of the top bar, this button was previously labelled "Activities". In GNOME 45, this static label has been replaced with a dynamic workspace indicator.

The new button design is a response to user testing results over recent years, and we are confident that its purpose will be more obvious to new GNOME users. As part of this change, the old app menu (which showed the name of the currently focused app), was also retired. This retirement was another response to user testing results, and was required to make space for the new design.

## Fast Search

Search performance has been a major area of work for GNOME 45. These performance improvements have been made to a range of apps, including **Software**, **Characters**, **Clocks**, **Files**, and **Calculator**. Together, these changes result in faster search and reduced resource usage, which can be seen in both individual apps and in system search.

## Camera Usage Indicator

A new camera indicator is a welcome addition to the system top bar in GNOME 45. This shows when a camera is in use, and joins the pre-existing microphone indicator, whose style has also been updated for this release.

The camera usage indicator is an important privacy feature, which joins other GNOME privacy features, including indicators for screen sharing and recording, app permissions, and device security settings.

The camera usage indicator is only shown for apps using the latest camera technologies (namely, [Pipewire](https://pipewire.org/)).

## New System Features

![Super S](super-s.webp)

GNOME 45 also includes a collection of new system features and enhancements. These include:

* **Keyboard backlight quick setting**: the quick settings menu now includes a button for controlling keyboard backlights.
* **Efficient video playback**: when possible, GNOME will now use hardware resources when playing videos. This is faster and uses less power.
* **New quick settings keyboard shortcut**: you can now use the new *Super+S* key combination to open and close the quick settings menu.
* **Updated pointer visuals**: GNOME's pointers (also known as cursors) have a great new look for GNOME 45.
* **Light system style**: GNOME has a new option to give the system a light interface style, as opposed to the default dark look. This can be enabled using [an extension](https://extensions.gnome.org/extension/6198/light-style/) or from the command line with `gsettings set org.gnome.desktop.interface color-scheme prefer-light`. Further integration is being investigated for future releases.
* **New wallpapers**: two beautiful new images were added to GNOME's collection of backgrounds.
* **Wayland support for [Input Leap](https://github.com/input-leap/input-leap)**: this provides a software implementation of KVM-switch like functionality, allowing multiple computers to be controlled from a single keyboard and mouse. GNOME 45 allows this software to be used with modern Wayland sessions.
* **Smoother pointer movement**: performance improvements mean that the pointer will move smoothly even when the computer is busy.

## New Image Viewer

GNOME 45 introduces a new default image viewer app. It’s fast and clean, making it a great way to quickly view images. Flipping from one image to the next is smooth and seamless, as are the visuals for zooming and rotation.

<video controls muted class="rounded">
  <source src="loupe.webm" type="video/webm">
  <source src="loupe.mp4" type="video/mp4">
</video>

The new image viewer is fully adaptive and supports mobile form factors. It can easily be used with gestures on a touchscreen or touchpad: two finger swipes navigate backwards and forwards, pinch zooms an image, and two fingers can also be used to rotate.

Other highlights of the new image viewer include an image properties sidebar, buttons to easily copy or trash images as you view them, and a printing window which allows easy control over layout.

## New Camera App

In addition to the new image viewer, GNOME 45 also includes a new camera app. This has an extremely clean and modern UI, with all the controls overlaid over the viewing area. Like the new image viewer, the new camera app works great on both mobile and desktop.

![Snapshot, the new Camera app](snapshot-screenshot.webp)

The new camera app comes with a range of essential features, including the ability to take still images or videos, a button to switch between different cameras, and a built-in viewing function, so you can quickly look at the images and videos that have just been taken.

## New Style Apps

Many core GNOME apps are using new interface components in the 45 release. These new components bring a fresh new look, and also perform better across different window sizes and device form factors.

One distinctive style change are new sidebars which occupy the full height of their windows. These look great, and they are also adaptive: change the width of the window, and the app layout automatically adjusts. The new sidebars can be seen in **Calendar**, **Characters**, **Clocks**, **Contacts**, **Files**, and **Settings**.

Many core apps also have an updated header bar style for GNOME 45, including **Console**, **Text Editor**, **Disk Usage Analyzer**, **Fonts**, **Tour**, and **Web**. These now have a subtle shadow that separates the header from the content below.

These more prominent style changes are accompanied by many other small style updates, which can be particularly found in the **Calendar**, **Maps**, **Files**, and **Calculator** apps.

- [![Console](https://apps.gnome.org/icons/scalable/org.gnome.Console.svg){: .icon-dropshadow} <br>Console](https://apps.gnome.org/Console/)
- [![Text Editor](https://apps.gnome.org/icons/scalable/org.gnome.TextEditor.svg){: .icon-dropshadow} <br>Text Editor](https://apps.gnome.org/TextEditor/)
- [![Calendar](https://apps.gnome.org/icons/scalable/org.gnome.Calendar.svg){: .icon-dropshadow} <br>Calendar](https://apps.gnome.org/Calendar/)
- [![Characters](https://apps.gnome.org/icons/scalable/org.gnome.Characters.svg){: .icon-dropshadow} <br>Characters](https://apps.gnome.org/Characters/)
- [![Clocks](https://apps.gnome.org/icons/scalable/org.gnome.clocks.svg){: .icon-dropshadow} <br>Clocks](https://apps.gnome.org/Clocks/)
- [![Contacts](https://apps.gnome.org/icons/scalable/org.gnome.Contacts.svg){: .icon-dropshadow} <br>Contacts](https://apps.gnome.org/Contacts/)
- [![Disk Usage Analyzer](https://apps.gnome.org/icons/scalable/org.gnome.baobab.svg){: .icon-dropshadow} <br>Disk Usage Analyzer](https://apps.gnome.org/Baobab/)
- [![Files](https://apps.gnome.org/icons/scalable/org.gnome.Nautilus.svg){: .icon-dropshadow} <br>Files](https://apps.gnome.org/Nautilus/)
- [![Fonts](https://apps.gnome.org/icons/scalable/org.gnome.font-viewer.svg){: .icon-dropshadow} <br>Fonts](https://apps.gnome.org/FontViewer/)
- [![Tour](https://apps.gnome.org/icons/scalable/org.gnome.Tour.svg){: .icon-dropshadow} <br>Tour](https://apps.gnome.org/Tour/)
- [![Web](https://apps.gnome.org/icons/scalable/org.gnome.Epiphany.svg){: .icon-dropshadow} <br>Web](https://apps.gnome.org/Epiphany/)
{: .icon-grid}

## Files Improvements

Much work has also been done on search in the **Files** app. This has been given a significant speed boost, meaning that results are now returned in milliseconds, even when searching millions of files. Other improvements to file search include:

* Faster and more seamless display of search results.
* More useful and predictable ordering of search results.
* New buttons to allow expanding the scope of a search.

<picture>
    <source srcset="search-screenshot-dark.png" media="(prefers-color-scheme: dark)">
    <img src="search-screenshot.png">
</picture>

Other enhancements in the new version of Files include a new window to choose which columns are shown in list view, more consistent date and time formats in list view, indicators for starred files in the icon view, and faster thumbnail generation.

## Improved Settings

Settings includes a range of improvements in GNOME 45:

 * The **Date & Time** settings now include options to configure the format of the time and date in the top bar.
 * Many settings are now also easier to understand, with descriptions added to all of the **Privacy** and **Sharing** subpages, and information bubbles added to the autologin and administrator user settings.
 * Accessibility was enhanced with better keyboard navigation from the search entry. Additionally, more windows can now be closed with the *Esc* key.
 * Many settings panels were given design polish, including a new *System Details* window in the **About** settings, and a new keyboard layout viewer which can be opened from the **Keyboard** settings.

<picture>
    <source srcset="settings-date-format-screenshot-dark.png" media="(prefers-color-scheme: dark)">
    <img src="settings-date-format-screenshot.png">
</picture>

## Enhanced Apps

GNOME's other core apps contain many new features and improvements for version 45. 

In **Software**:
 
  * When removing a Flatpak, an option is now shown to allow deleting the app's data.
  * When there aren't many apps available, the app browsing UI is adjusted to compensate.
  * Downloading and installing updates is avoided while games are running.

In **Calendar**:
 
  * The month view can now be smoothly scrolled one line at a time, as opposed to having to flip from month to month. This makes it possible to browse your calendar without losing track of where you are, and provides more flexibility over what dates and events are shown at any one time.
  * New keyboard shortcuts have been added for synchronizing calendars (F5), opening the calendars menu (F8), and managing calendars (Ctrl+Alt+M).
  * Search now returns events from a wider range of dates.
  * There have also been a range of performance improvements.

In **Maps**:

 * A button has been added that indicates direction and allows orientating the map to north.
 * It is now also possible to explore nearby points of interest from the search popover.
 * The zoom buttons are now overlaid over the map, as opposed to being in the header bar.

Other app enhancements include:

 * **Connections** now allows copying text, images, and files to and from remote computers when using RDP connections.
 * **Console**, the GNOME terminal app, has a new preferences window, with options for setting a custom font and disabling the system bell.
 * **Calculator** can convert additional currencies, including the Taiwanese Dollar, Ukrainian Hryvnia, Nigerian Naira, and Jamaican Dollar.


## New Circle Apps

[GNOME Circle](https://circle.gnome.org/) is a group of fantastic apps for GNOME, which the GNOME project promotes and supports. Since GNOME 44 was released, six new apps have been added. These include:

 * [Telegraph](https://apps.gnome.org/Telegraph/), a morse code translator.
 * [Cartridges](https://apps.gnome.org/Cartridges/), a game launcher which supports Steam, Lutris, Heroic, and more.
 * [Ear Tag](https://apps.gnome.org/EarTag/), an audio file tag editor.
 * [Paper Clip](https://apps.gnome.org/PdfMetadataEditor/), a PDF metadata editor.
 * [Forge Sparks](https://apps.gnome.org/ForgeSparks/), a notifier app for Github, Gitea, and Forgejo.
 * [Impression](https://apps.gnome.org/Impression/), which writes disk images to removable drives.

## Developer Experience

GNOME 45 includes new features and improvements for developers who are using the GNOME platform. Read the developers section to learn more.

<ul class="linkdiv">
  <li>
    <a href="developers/index.html"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 45

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 45 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using the [Boxes](https://apps.gnome.org/Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](http://welcome.gnome.org).

