---
layout: default
image: 46.png
urlprepend: "../../"
---

<picture class="full">
    <source srcset="developers-dark.svg" media="(prefers-color-scheme: dark)">
    <img src="developers.svg">
</picture>

# What's new for developers

GNOME 46 comes with plenty of new features and enhancements for those who use the GNOME platform. These include updates to GNOME's developer tools, improved libraries, and updated language bindings.

We encourage you to explore the [GNOME Developer website](https://developer.gnome.org/) for a comprehensive overview of the resources available to you. Get started building and contributing to the next generation of GNOME experiences!

## GTK

GNOME 46 has been released in coordination with the latest GTK version, 4.14, which comes with several significant changes.

### New default renderer

This latest GTK release has a new default renderer, called NGL. Developers will notice a number of improvements with the new renderer, including enhanced antialiasing, better handling of fractional scaling, more powerful support for gradients, and improved performance using Dmabufs. 

NGL is one of two new GTK renderers which are currently being worked on, and which mark a significant step towards a more powerful and versatile rendering future for GTK applications. For those who want to learn more, there are two posts on the GTK development blog: [new renderers for GTK](https://blog.gtk.org/2024/01/28/new-renderers-for-gtk/) and [on fractional scales, fonts and hinting](https://blog.gtk.org/2024/03/07/on-fractional-scales-fonts-and-hinting/).

### Accessibility improvements

GTK 4.14 comes with a set of significant accessibility improvements.

* **Accessible text elements**: the new [AccessibleText interface](https://docs.gtk.org/gtk4/iface.AccessibleText.html) provides the ability to add accessibility information to widgets like text editors and terminal emulators.
* **In-app notifications**: applications that send messages via in-app notifications, like [AdwToast](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Toast.html) in libadwaita, can now announce those notifications to assistive technologies using [gtk_accessible_announce()](https://docs.gtk.org/gtk4/method.Accessible.announce.html)
* **Bridging accessibility trees**: libraries such as WebKit with their own accessibility trees can now bridge them with the accessibility tree provided by GTK.

Again, more information can be found on the [GTK development blog](https://blog.gtk.org/2024/03/08/accessibility-improvements-in-gtk-4-14/).

<picture class="full">
    <img src="gtk.svg">
</picture>

## Libadwaita

The main addition in libadwaita 1.5 is the new dialogs (`AdwDialog`), which are presented within the parent window rather than as their own window, and are adaptive, presented as bottom sheets on mobile. Ports of existing window-based dialog widgets are available (`AdwAlertDialog`, `AdwPreferencesDialog`, `AdwAboutDialog`). The old widgets are planned for deprecation next next cycle.

In addition to the new dialogs, libadwaita 1.5 includes a set of the new APIs:

| Class | New method/property |
|--------|---------------------|
| EntryRow | [:text-length](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/property.EntryRow.text-length.html) |
| BreakpointBin | [remove_breakpoint()](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/method.BreakpointBin.remove_breakpoint.html) |
| SwipeTracker  | [:allow-window-handle](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.SwipeTracker.html) |

A [detailed blog post](https://blogs.gnome.org/alicem/2024/03/15/libadwaita-1-5/) is available.

## WebKitGTK

The following web features now function in sandboxed Flatpak applications:

* Gamepad API (needs `--device=input` permission currently)
* Geolocation API
* Drag and drop of files from the system to a `WebKit.WebView`

The accessibility trees of the `WebView` and GTK4 are also now connected.

### Other GNOME Platform Enhancements

Other improvements to the GNOME platform for GNOME 46 include:

* **GLib**: now generates its own introspection data, which results in faster build times, and is used to publish [online API docs](https://docs.gtk.org/glib/).
* **Platform/SDK**: a WebP loader is included in the latest version of the GNOME Flatpak SDK and runtime, bringing built-in WebP image format support for apps.
* **Tracker**: has added API support for language-tagged strings, with the addition of [tracker_sparql_statement_bind_langstring()](https://gnome.pages.gitlab.gnome.org/tracker/method.SparqlStatement.bind_langstring.html) and [tracker_sparql_cursor_get_langstring()](https://gnome.pages.gitlab.gnome.org/tracker/method.SparqlCursor.get_langstring.html). Database integrity checking has also been enhanced.
* **libmanette**: gamepads enumeration and monitoring is now functional in Flatpak applications.

## Enhanced Developer Apps

GNOME 46 includes a number of enhancements to GNOME's developer apps.

* **VTE**: significant performance improvements to VTE can be seen in all of GNOME's terminal apps (GNOME Terminal, Console, Ptyxis). By optimizing internal processes, wall clock time for common tasks has been reduced by a substantial 40%. Further optimizations utilizing native GTK 4 drawing primitives are planned, aiming to specifically decrease draw latency on GTK 4 systems without impacting GTK 3 performance.
* **System Monitor**: has been ported to GTK 4, for a modern look and feel. A Disk resource usage graph has also been added.
* **New third-party apps**: two new interesting developer apps have been released during the GNOME 46 development cycle. The first, called Ptyxis, is a new terminal app, designed and built with container-based use cases in mind ([read the blog post](https://blogs.gnome.org/chergert/2024/02/)). Second, Biblioteca is a new docs app which provides offline access and has a great UI ([find it on Flathub](https://flathub.org/apps/app.drey.Biblioteca)).

## Workbench

Workbench 46 makes it easy to try, learn and explore GNOME app development, and is a great way to try new GNOME 46 developer features such as libadwaita's new style dialogs (check out the "Dialog" and "Message Dialogs" entries in the Library).

<a href='https://flathub.org/apps/re.sonny.Workbench'>
<img width='120' alt='Download on Flathub' src='https://flathub.org/api/badge?locale=en'/>
</a>

Highlights of changes between Workbench 45 and 46 include:

* Python support added with formatter, diagnostics and 90 demos available
* Blueprint and Vala code now auto formats on run
* The Library shows which languages are available for each demo
* Documentation was moved to a standalone application - [Biblioteca](https://flathub.org/apps/app.drey.Biblioteca)
* Greatly reduced download and install size
* “Extensions” window with instructions on how to enable Vala and Rust support
* Shows inline diagnostics for Rust
* Demos have been updated for GNOME 46
* 4 new demos for a total of 106: "CSS Gradients", "Context Menu", "HTTP Server", "Snapshot"
* Increased language coverage:
   * 90 demos made available in Python
   * 10 new demos available in Rust for a total of 45
   * 27 new demos available in Vala for a total of 59

### Notes for Distributions

Distributions which plan to include GNOME 46 should be aware of the following changes:

* **Mutter** now has a reduced dependency footprint. See more details in the [blog post](https://belmoussaoui.com/blog/18-reducing-mutter-dependencies/).
* **GLib** now generates its own introspection data, instead of deferring it to gobject-introspection. This change introduces a cyclic dependency between GLib and gobject-introspection that can be solved with a 2-phase build; for more information, see [the topic on Discourse](https://discourse.gnome.org/t/dealing-with-glib-and-gobject-introspection-circular-dependency/18701).
* **gnome-software** [now has AppStream merging capabilities](https://gitlab.gnome.org/GNOME/gnome-software/-/issues/1649), allowing app metadata to be merged from different sources, which can be useful for modifying the presentation of the explore experience.
* **librsvg** no longer uses gdk-pixbuf to load images, but still provides the gdk-pixbuf loader for SVG images. Please file bugs in the librsvg bug tracker if issues are encountered, such as different performance or memory usage, or unsupported image variants.
* **gnome-keyring** [has deprecated the ssh component](https://gitlab.gnome.org/GNOME/gnome-keyring/-/merge_requests/60). It is now recommended to use gcr-ssh-agent and not gnome-keyring ssh.

